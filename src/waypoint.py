import carla
import random

client = carla.Client('localhost', 2000)
client.load_world('Town04')
world = client.get_world()
map = world.get_map()
# Get all waypoints in the map
waypoints = map.generate_waypoints(distance=5)  # generates a waypoint every 10 meters
spawn_points = world.get_map().get_spawn_points()

# For each waypoint, draw a small red point
for waypoint in waypoints:
    if waypoint.is_intersection:
        world.debug.draw_point(waypoint.transform.location, size=0.4, color=carla.Color(255, 0, 0), life_time=0)
        location = waypoint.transform.location
        world.debug.draw_string(location, 'x={}, y={}, z={}'.format(location.x, location.y, location.z), draw_shadow=False,
                            color=carla.Color(r=255, g=255, b=255), life_time=100, persistent_lines=True)
        
for i, spawn_point in enumerate(spawn_points):
    world.debug.draw_string(spawn_point.location, str(i), draw_shadow=False,
                            color=carla.Color(r=255, g=255, b=255), life_time=100, persistent_lines=True)
# Wait for a while so the map can be observed
