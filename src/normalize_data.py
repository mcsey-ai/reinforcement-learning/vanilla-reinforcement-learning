import pandas as pd

# Read the data from the CSV file
data = pd.read_csv('training_data_global.csv')

from sklearn.preprocessing import StandardScaler

# Create a StandardScaler object
scaler = StandardScaler()

# Fit the scaler to the data
scaler.fit(data)

# Transform the data
normalized_data = scaler.transform(data)

import matplotlib.pyplot as plt

# Plot the normalized data
plt.plot(normalized_data)

# Add labels and title
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Normalized Data')

# Show the plot
plt.show()
