import csv
import random

# Assume you have a list of episodes and corresponding average_rewards
episodes = list(range(2790, 5001))  # example episodes
average_rewards = [random.uniform(29.82534600, 30.453763463) for _ in range(2210)]  # randomly generated example average rewards

# Create a CSV file and write the data
with open('2790.csv', 'w', newline='') as csvfile:
    fieldnames = ['episode', 'avg_pci']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for e, r in zip(episodes, average_rewards):
        writer.writerow({'episode': e, 'avg_pci': r})
