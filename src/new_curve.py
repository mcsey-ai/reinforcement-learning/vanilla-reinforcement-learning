import pandas as pd
import matplotlib.pyplot as plt

# Read data from CSV
data = pd.read_csv('pci_final.csv')

# Extract episodes and values
episodes = data['episode']
values1 = data['avg_pci_localdriver'].rolling(window=500).mean()
values2 = data['avg_pci'].rolling(window=500).mean()

# Plotting
plt.figure(figsize=(10, 6))
plt.plot(episodes, values1, label="Local experience relay buffer MADDPG", color="blue")
plt.plot(episodes, values2, label="Global experience relay buffer MADDPG", color="red")
plt.title("Comparison of Global versus Local experience relay buffer MADDPG models", fontsize=16)
plt.xlabel("Episode", fontsize=14)
plt.ylabel("Average PCI", fontsize=14)
plt.legend()
plt.grid(False)
plt.savefig('final_training_pci.png',fontsize=14)
plt.show()
