import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

episodes = np.arange(1, 5001)
values = np.zeros(len(episodes))

# Linearly descending values for the first 1507 episodes
negative_range = np.linspace(-40, -1, 507)
values[:507] = negative_range

# Logarithmic curve for smooth increase from 1508th to 3780th episode
x_range = np.linspace(0, 1, 2780 - 507)
log_values = np.log(x_range * 20 + 1)
scaled_log_values = (log_values - np.min(log_values)) * (30 - (-1)) / (np.max(log_values) - np.min(log_values)) - 1

values[507:2780] = scaled_log_values

# Flat value after 3780th episode
values[2780:] = values[2779]

# Save to CSV
df = pd.DataFrame({
    'Episode': episodes,
    'Value': values
})
df.to_csv('smooth_curve_values_second.csv', index=False)

# Plotting the curve
plt.figure(figsize=(10, 6))
plt.plot(episodes, values, label="Curve")
plt.axvline(x=2507, color='blue', linestyle='--', label='1507th episode')
plt.axvline(x=3780, color='red', linestyle='--', label='3780th episode')
plt.title("Smooth Curve with Negative Initial Values and Flat Ending")
plt.xlabel("Episode")
plt.ylabel("Value")
plt.legend()
plt.grid(True)
plt.ylim(-42, 32)  # Setting y-axis limits to show values clearly
plt.show()
