import pandas as pd
import numpy as np
from scipy import stats

# Read the data from the CSV file
data_no_relay = pd.read_csv('results/average_rewards_without_relay/avg_rewards.csv')
data_relay = pd.read_csv('results/average_rewards_relay_buffer/avg_rewards.csv')

column1 = data_relay['average_reward']
column2 = data_no_relay['average_reward']

# Assuming that the column you want to calculate the variance for is named 'column_name'
# Replace 'column_name' with the actual column name in your CSV file
column_variance = np.var(column1)
column_mean  = column1.mean()
column_median = column1.median()

# Calculate the standard deviation and the size of the dataset
std_dev = np.std(column1)
N = len(column1)

# Calculate the confidence interval for a confidence level of 95%
z = stats.norm.ppf(0.975)  # 1.96 for 95% confidence
margin_error = z * (std_dev / np.sqrt(N))
confidence_interval = (column_mean - margin_error, column_mean + margin_error)


print("Variance of the average reward for shared expereince: ", column_variance)
print("Mean of the average reward for shared expereince: ", column_mean)
print("Median of the average reward for shared expereince: ", column_median)
print("95% Confidence interval for the average reward for shared expereince: ", confidence_interval)

column_variance = np.var(column2)
column_mean  = column2.mean()
column_median = column2.median()

# Calculate the standard deviation and the size of the dataset
std_dev = np.std(column2)
N = len(column2)

# Calculate the confidence interval for a confidence level of 95%
z = stats.norm.ppf(0.975)  # 1.96 for 95% confidence
margin_error = z * (std_dev / np.sqrt(N))
confidence_interval = (column_mean - margin_error, column_mean + margin_error)


print("Variance of the average reward for individual expereince: ", column_variance)
print("Mean of the reward for individual expereince: ", column_mean)
print("Median of the reward for individual expereince: ", column_median)
print("95% Confidence interval for the reward for individual expereince: ", confidence_interval)

u_statistic, p_value = stats.mannwhitneyu(column1, column2)

print("\nMann-Whitney U test")
print("U statistic: ", u_statistic)
print("P-value: ", p_value)
