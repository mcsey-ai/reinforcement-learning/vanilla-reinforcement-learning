import carla
import random
import math
import numpy as np

def calculate_steering(vehicle_transform, waypoint):
    # Calculate the orientation of the vehicle in radians
    vehicle_orientation = math.radians(vehicle_transform.rotation.yaw)

    # Create a normalized vector from the vehicle's location to the location of the waypoint
    planned_vector = np.array([waypoint.transform.location.x - vehicle_transform.location.x,
                               waypoint.transform.location.y - vehicle_transform.location.y])
    planned_orientation = math.atan2(planned_vector[1], planned_vector[0])

    # Calculate the steering angle
    steering_angle = planned_orientation - vehicle_orientation
    # Make sure the steering angle is between -1 and 1
    steering_angle = np.clip(steering_angle, -1, 1)

    return steering_angle


def distance_2d(location1, location2):
    dx = location1.x - location2.x
    dy = location1.y - location2.y
    return math.sqrt(dx**2 + dy**2)

def control_vehicle(snapshot, control):
    timestamp = snapshot.timestamp
    print("Timestamp: {} seconds, {} platform time, {} elapsed seconds".format(
    timestamp.seconds, timestamp.platform_timestamp, timestamp.elapsed_seconds))
    control.throttle = 0.2  # You may want to adjust this 
    control.steer = 0.0
    vehicle.apply_control(control)

client = carla.Client('localhost', 2000)
client.load_world('Town04')
world = client.get_world()
map = world.get_map()
client.set_timeout(60.0)
client.start_recorder("recording.log")

settings = world.get_settings()
settings.synchronous_mode  = True
settings.fixed_delta_seconds = 0.05
world.apply_settings(settings)

traffic_manager = client.get_trafficmanager()
traffic_manager.set_synchronous_mode(True)

#traffic_manager.set_random_device_seed(0)
random.seed(0)

spectator = world.get_spectator()

spawn_points = world.get_map().get_spawn_points()
blueprint_filter='model3'
blueprint_library = world.get_blueprint_library()
prop_bp = random.choice(blueprint_library.filter('static.prop.briefcase'))
prop_waypoint = map.get_waypoint(carla.Location(x=201, y=-173, z=0.19589))
spawn_transform = prop_waypoint.transform

blueprint_library = world.get_blueprint_library()
vehicle_bp = blueprint_library.filter(blueprint_filter)[0]
#vehicle_width = vehicle_bp.get_attribute('width').as_float()
#vehicle_length = vehicle_bp.get_attribute('length').as_float()
possible_agent_route_ind ={0: [276, 166, 165, 309, 163]}
route_1 = []
# Define your waypoints somehow


for i, spawn_point in enumerate(spawn_points):
    world.debug.draw_string(spawn_point.location, str(i), life_time=1000)

for ind in possible_agent_route_ind:
    route_1.append(spawn_points[ind].location)

vehicle = world.try_spawn_actor(vehicle_bp, spawn_points[276])
bounding_box = vehicle.bounding_box
vehicle_length = bounding_box.extent.x * 2
vehicle_width = bounding_box.extent.y * 2
vehicle_transform= vehicle.get_transform()
start_location = vehicle_transform.location
start_waypoint = map.get_waypoint(start_location)
waypoints = start_waypoint.next(5)
#waypoints = map.generate_waypoints(distance=5)
target_waypoint = waypoints.pop(0)
if vehicle: # IF vehicle is succesfully spawned
    vehicle.set_autopilot(False) # Give TM control over vehicle
    prop = world.spawn_actor(prop_bp, spawn_transform)

    # Set parameters of TM vehicle control, we don't want lane changes
    # traffic_manager.random_left_lanechange_percentage(vehicle, 0)
    # traffic_manager.random_right_lanechange_percentage(vehicle, 0)
    # traffic_manager.auto_lane_change(vehicle, False)
    # traffic_manager.set_path(vehicle, route_1)
control = carla.VehicleControl()
while True:
    world.tick()
    #world.on_tick(lambda world_snapshot: control_vehicle(world_snapshot, control))
    current_transform = vehicle.get_transform()
    #print(vehicle.get_transform())
    current_location = current_transform.location
    current_waypoint = map.get_waypoint(current_location)
    target_location = target_waypoint.transform.location

    lane_width = current_waypoint.lane_width
    if current_waypoint.is_intersection:
        print(f'Crossing intersection')
        control.throttle = 0.0 # You may want to adjust this 
        control.steer = 0.0
        vehicle.apply_control(control)
    # Calculate the steering angle needed to get to the target waypoint
    #steering_angle = calculate_steering(current_location, target_location, current_transform.rotation.yaw)
    
    # Apply control to the vehicle
    else:
        control.throttle = 0.2  # You may want to adjust this 
        control.steer = 0.0
        vehicle.apply_control(control)

    # If the vehicle is close to the target waypoint, switch to the next waypoint
    tolerance_distance = vehicle_width/2 + lane_width/2
    #print(current_location.distance(target_location))
    #if current_location.distance(target_location) < 5:  # 1 meter tolerance
    #if current_location.distance(target_location) < tolerance_distance:
    if target_waypoint.is_intersection:
        print(f'moved a tick')
        if waypoints:
            target_waypoint = waypoints.pop(0)   
            print(f'Crossing intersection')
        else:
            print("Reached the end of the path")
            client.stop_recording()
            break
    