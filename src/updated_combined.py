import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file
df = pd.read_csv('evaluation_comfort_index_ego_2.csv')

# Create a figure
plt.figure()

# Plot Ego_1_x
plt.plot(df['Episode'], df['Ego_2_x'], label='Ego 2 vehicle', color='b')

# Plot Ego_1_y
plt.plot(df['Episode'], df['Ego_2_y'], label='Ego 2 vehicle', color='r')

plt.xlabel('Episode')
plt.ylabel('Values')
plt.title('Avg Evaluation Comfort Index over 500 Episodes')
plt.legend()

# Save the figure
plt.savefig('evaluation_comfort_index_ego_2.png')

# Display the figure
plt.show()
