import carla
import random
import logging
import time, timeit

actor_list = []
camera = None
try:
    client = carla.Client('localhost', 2000)
    world = client.get_world()

    settings = world.get_settings()
    settings.synchronous_mode  = True
    settings.fixed_delta_seconds = 0.05
    world.apply_settings(settings)

    traffic_manager = client.get_trafficmanager()
    traffic_manager.set_synchronous_mode(True)

    traffic_manager.set_random_device_seed(0)
    random.seed(0)

    spectator = world.get_spectator()
    spawn_points = world.get_map().get_spawn_points()
    logging.info(f'Spawn points: {spawn_points}')
    spawn_indices = [75, 2, 120, 4, 5, 11, 13, 7, 6, 3, 77, 80]
    vehicle_blueprints = world.get_blueprint_library().filter('*vehicle*')
    sensor_blueprints = world.get_blueprint_library().find('sensor.camera.depth')
    
    for i, spawn_point in enumerate(spawn_points):
        world.debug.draw_string(spawn_point.location, str(i), life_time=10)

    ego_bp = vehicle_blueprints.find('vehicle.lincoln.mkz_2020')
    ego_bp.set_attribute('role_name', 'hero')
    ego_vehicle = world.spawn_actor(ego_bp, spawn_points[125])

    # Let's add now a "depth" camera attached to the vehicle. Note that the
    # transform we give here is now relative to the vehicle.
    #camera_bp = blueprint_library.find('sensor.camera.depth')
    camera_transform = carla.Transform(carla.Location(x=0.15, y=-0.4, z=1.2), carla.Rotation())
    camera = world.spawn_actor(sensor_blueprints, camera_transform, attach_to=ego_vehicle)
    actor_list.append(camera)
    print('created %s' % camera.type_id)

    # Now we register the function that will be called each time the sensor
    # receives an image. In this example we are saving the image to disk
    # converting the pixels to gray-scale.
    cc = carla.ColorConverter.LogarithmicDepth
    camera.listen
    (lambda image: image.save_to_disk('out/%06d.png' % image.frame, cc))
    vehicles = []
    vehicles.append(ego_vehicle)
    # for ind in spawn_indices:
    #     vehicle = world.try_spawn_actor(random.choice(blueprints), spawn_points[ind])
        
    #     vehicles.append(vehicle)

    for vehicle in vehicles:
        vehicle.set_autopilot(True)
    actor_list.append(vehicles)
    while True:
        world.tick()
finally:
    print('destroying actors')
    client.apply_batch([carla.command.DestroyActor(x) for x in actor_list])
    print('done.')
    #max_vehicles = 10