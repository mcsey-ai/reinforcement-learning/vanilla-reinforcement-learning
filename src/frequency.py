import matplotlib.pyplot as plt
import pandas as pd

# Read data from CSV file
data = pd.read_csv('data_without.csv')

# Create a histogram
plt.hist(data['global_experience'])

# Add labels and title
plt.xlabel('Value')
plt.ylabel('Frequency')
plt.title('PCI Frequency Distribution')

# Show the plot
plt.savefig('global_experience_without_driver.png')
plt.show()

# import matplotlib.pyplot as plt
# import pandas as pd

# # Read data from CSV file
# data = pd.read_csv('data_rogue.csv')

# # Create a histogram for the first column
# plt.hist(data['local_experience'], bins=10, alpha=0.5, label='Local Experience')

# # Create a histogram for the second column
# plt.hist(data['global_experience'], bins=10, alpha=0.5, label='Global Experience')

# # Add labels and title
# plt.xlabel('Value')
# plt.ylabel('Frequency')
# plt.title('Frequency Distribution')

# # Add a legend
# plt.legend(loc='upper right')

# # Show the plot
# plt.show()
