import csv
import matplotlib.pyplot as plt
import os

# with open('data_without_rogue.csv', 'r') as input_file, open('__output.csv', 'w') as output_file:
#     # Iterate over the lines in the input file
#     for line in input_file:
#         # Check if the line is not empty
#         if line.strip():
#             # Write the line to the output file
#             output_file.write(line)

# # Rename the output file to the original file name

# os.rename('__output.csv', 'data_without.csv')

# Open the CSV file
with open('data_rogue.csv', 'r') as f:
    # Create a CSV reader object
    reader = csv.reader(f)
    
    # Skip the header row
    next(reader)
    
    # Extract columns
    episodes = []
    avg_pci_norogue_driver = []
    avg_pci_rogue_driver = []
    for row in reader:
        episodes.append(int(row[0]))
        avg_pci_norogue_driver.append(float(row[1]))
        avg_pci_rogue_driver.append(float(row[2]))

# Plot data
plt.figure(figsize=(15, 8))
plt.scatter(episodes, avg_pci_norogue_driver, label='Presence of Adversarial Driver for Local Experience Agents')
plt.scatter(episodes, avg_pci_rogue_driver, label='Presence of Adversarial Driver for Global Experience Agents')
plt.legend()
plt.title('PCI Comparision for Local Experienced Trained verses and Global Experience Trained Agents in Presence of Adversarial Driver')
plt.xlabel('Episode')
plt.ylabel('Average PCI over 1000 Evaluation Episodes')
plt.savefig('pci_with_rogue_driver.png')
plt.show()

