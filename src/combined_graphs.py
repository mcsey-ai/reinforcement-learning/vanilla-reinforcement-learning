import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file
df = pd.read_csv('training_data_global.csv')

df['avg_pci'] = df['avg_pci']
df['average_reward'] = df['average_reward']

# Create a figure and a set of subplots
fig, ax1 = plt.subplots()

# Plot Ego_1_x
ax1.plot(df['episode'], df['avg_pci'], color='b')
ax1.set_xlabel('episode')
ax1.set_ylabel('Progress of reward accumulation during training with global experiences', color='b')
ax1.tick_params('y', colors='b')

# Create a second y-axis for the same x-axis
ax2 = ax1.twinx()
ax2.plot(df['episode'], df['average_reward'], color='r')
ax2.set_ylabel('Progress of PCI during taining with global experiences', color='r')
ax2.tick_params('y', colors='r')

fig.tight_layout()

# Save the figure
plt.savefig('pci_ind_scatter.png')

# Display the figure
plt.show()
