import pandas as pd
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

# Load the data from the CSV file
data = pd.read_csv('data_shared.csv')

# Extract the columns to use for clustering
X = data[[ 'without_rogue']]

# Create a k-means model with 3 clusters
kmeans = KMeans(n_clusters=3)

# Fit the model to the data
kmeans.fit(X)

# Get the cluster assignments for each data point
labels = kmeans.labels_

# Plot the data points with different colors for each cluster
plt.scatter(X['with_rogue'], X['without_rogue'], c=labels)
plt.show()
