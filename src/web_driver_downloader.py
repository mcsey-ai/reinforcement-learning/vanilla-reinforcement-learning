import os
import urllib.request

from selenium import webdriver


def get_webdriver(driver_name: str):
    """
    Download the appropriate Selenium webdriver based on the driver name and operating system.
    """

    if driver_name == 'chromedriver':
        try:
            if os.name == 'nt':
                url = 'https://chromedriver.storage.googleapis.com/LATEST_RELEASE'
                response = urllib.request.urlopen(url)
                version_number = response.read().decode().strip()

                driver_url = f'https://chromedriver.storage.googleapis.com/{version_number}/chromedriver_win32.zip'
                urllib.request.urlretrieve(driver_url, 'chromedriver_win32.zip')
                os.system('powershell Expand-Archive chromedriver_win32.zip -DestinationPath .')
                os.remove('chromedriver_win32.zip')

                return webdriver.Chrome()
            if os.name == 'posix':
                url = 'https://chromedriver.storage.googleapis.com/LATEST_RELEASE'
                response = urllib.request.urlopen(url)
                version_number = response.read().decode().strip()

                driver_url = f'https://chromedriver.storage.googleapis.com/{version_number}/chromedriver_linux64.zip'
                urllib.request.urlretrieve(driver_url, 'chromedriver_linux64.zip')
                os.system('unzip chromedriver_linux64.zip')
                os.remove('chromedriver_linux64.zip')

                return webdriver.Chrome()
        except:
            raise Exception('Unsupported operating system')
    if driver_name == 'geckodriver':
        try:
            if os.name == 'nt':
                url = 'https://api.github.com/repos/mozilla/geckodriver/releases/latest'
                response = urllib.request.urlopen(url)
                version_info = json.loads(response.read().decode())

                driver_url = f'https://github.com/mozilla/geckodriver/releases/download/{version_info["tag_name"]}/geckodriver-{version_info["tag_name"]}-win64.zip'
                urllib.request.urlretrieve(driver_url, 'geckodriver_win64.zip')
                os.system('powershell Expand-Archive geckodriver_win64.zip -DestinationPath .')
                os.remove('geckodriver_win64.zip')

                return webdriver.Firefox()
            if os.name == 'posix':
                url = 'https://api.github.com/repos/mozilla/geckodriver/releases/latest'
                response = urllib.request.urlopen(url)
                version_info = json.loads(response.read().decode())

                driver_url = f'https://github.com/mozilla/geckodriver/releases/download/{version_info["tag_name"]}/geckodriver-{version_info["tag_name"]}-linux64.tar.gz'
                urllib.request.urlretrieve(driver_url, 'geckodriver_linux64.tar.gz')
                os.system('tar xvzf geckodriver_linux64.tar.gz')
                os.remove('geckodriver_linux64.tar.gz')

                return webdriver.Firefox()
        except:
            raise Exception('Unsupported operating system')
    else:
        raise Exception('Unsupported driver')
