import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file
df = pd.read_csv('picindividual.csv')

df['avg_pci_norogue_driver'] = df['avg_pci_norogue_driver'].rolling(window=100).mean()
df['avg_pci_rogue_driver'] = df['avg_pci_rogue_driver'].rolling(window=100).mean()

# Create a figure and a set of subplots
fig, ax1 = plt.subplots()

# Plot 'presence_of_rogue_driver'
ax1.plot(df['episode'], df['avg_pci_rogue_driver'], color='b', label='Presence of Rogue Driver')

# Plot 'absence_of_rogue_driver'
ax1.plot(df['episode'], df['avg_pci_norogue_driver'], color='r', label='Absence of Rogue Driver')

ax1.set_xlabel('Episode')
ax1.set_ylabel('Avg Passenger Comfort Index over 1000 Evaluation Episodes Based on Individual Experiences')
ax1.legend()

fig.tight_layout()

# Save the figure
plt.savefig('combined_pci_individual.png')

# Display the figure
plt.show()
