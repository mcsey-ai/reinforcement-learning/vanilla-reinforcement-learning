import matplotlib.pyplot as plt

# Data to plot
rewards = [[51, 49], [73, 27],
           #[77, 23], [96, 4]
           ]

# Corresponding agents
agents = ['Local Experience \n Presence of Adversarial Driver', 
         'Global Experience \n Presence of Adversarial Driver', 
          #'Local Experiecne\n Absence of Rogue Driver', 
          #'Global Experience \n Absence of Rogue Driver'
          ]

# Colors for the bars
colors = ['blue', 'green']
plt.figure(figsize=(10,10))
for i, reward in enumerate(rewards):
    # Create a stacked bar chart for each pair
    plt.bar(agents[i], reward[0], color=colors[0])
    plt.bar(agents[i], reward[1], bottom=reward[0], color=colors[1])
    plt.text(agents[i], reward[0]/2, f"{reward[0]:.1f}%", ha='center', va='center', color='white')
    plt.text(agents[i], reward[0] + reward[1]/2, f"{reward[1]:.1f}%", ha='center', va='center', color='white')

# Label the axes
plt.xlabel('Algorithm Variant')
plt.ylabel('Completion Rates')
#plt.xticks(rotation='vertical'10
# Title of the chart
plt.title('Comparing Rate of Mission Completion of Local Expereince vrs Global Experience, Presence of an Adversarial Driver')

# Create a legend
plt.legend(["Successful mission completion", "Unsuccessful mission completion due to collision"], title="Values")

# Display the chart
plt.savefig("AdversarialDriver.png")
plt.show()
