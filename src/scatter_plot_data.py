import csv
# import random

# num_episodes = 1000
# data = []
# for episode in range(1, num_episodes + 1):
#     with_rogue_driver = round(random.uniform(0.65, 0.80), 2)
#     without_adversarial_driver = round(random.uniform(0.75, 0.85), 2)
#     data.append([episode, with_rogue_driver, without_adversarial_driver])

# # Write data to CSV file
# with open('_data.csv', 'w') as f:
#     writer = csv.writer(f)
#     writer.writerow(["episode", "with_rogue_driver", "with_rogue_driver"])
#     writer.writerows(data)

# print("Data written to _data.csv")

import random

num_episodes = 1000
data = set()
while len(data) < num_episodes:
    episode = len(data) + 1
    local_without_rogue = round(random.uniform(0.6500900, 0.751399), 6)
    global_without_rogue = round(random.uniform(0.810123, 0.93137), 6)
    data.add((episode, local_without_rogue, global_without_rogue))

# Write data to CSV file
with open('data_without_rogue.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerow(["episode", "local_experience", "global_experience"])
    writer.writerows(sorted(data))

print("Data written to data_without_rogue.csv")
