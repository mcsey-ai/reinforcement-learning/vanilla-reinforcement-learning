import matplotlib.pyplot as plt

# Data to plot
labels = 'Successful Completions', 'Unsuccessful Completions'
sizes = [51, 49]  # percentages for each section
colors = ['green', 'red']  # colors for each section

# Plot
plt.figure(figsize=(10,10))
plt.bar(sizes, labels=labels, colors=colors, autopct='%1.1f%%', startangle=140, height=100)
plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
plt.title('Rate of Successfully Reaching Destination with Presence of Rogue Driver')
plt.savefig('mission_completion_ind_with_rogue.png')
plt.show()
