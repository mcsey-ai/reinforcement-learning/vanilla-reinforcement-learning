import pandas as pd

# Load the two CSV files into pandas DataFrames
df1 = pd.read_csv('pci_training_data.csv', sep=',')
df2 = pd.read_csv('pci_training_shared.csv', sep=',')

# Make sure the 'Episode' column is the same type in both DataFrames
df1.dropna(subset=['episode'], inplace=True)
df1['episode'] = df1['episode'].astype(int)
df2['episode'] = df2['episode'].astype(int)

# Merge the two DataFrames on the 'Episode' column
df = pd.merge(df1, df2, on='episode')

# Save the merged DataFrame to a new CSV file
df.to_csv('pci_final.csv', index=False)
