import pandas as pd
import matplotlib.pyplot as plt

# Load the data from the csv file
df = pd.read_csv('data.csv', sep=',')

# Set the index to 'Episode'
# df.set_index('episode', inplace=True)

# # Create the plot
# plt.figure(figsize=(10,5))
# plt.plot(df['average_reward'], label='Average Rewards over 5000 episodes for shared experiences')

# # Set the title and labels
# plt.title('Average Comfort Index for Ego 2 during evaluation with rogue driver over 500 episodes')
# plt.xlabel('Episode')
# plt.ylabel('Average Comfort Index')

# # Add a legend
# plt.legend()

# # Save the figure to a file
# plt.savefig('combined_evaluations_collisions.png')

# # Show the plot
# plt.show()

df['average_pci_200'] = df['average_passenger_comfort_index'].rolling(window=100).mean()

# Plot the data
plt.figure(figsize=(10, 5))
plt.plot(df['episode'], df['average_pci_200'], label='Average Passenger Comfort Index per 100 Episodes')
plt.xlabel('Episodes')
plt.ylabel('Average Reward')
plt.title('Average Passenger Comfort Index during Training over 1000')
plt.legend()
plt.grid(True)
plt.savefig('pci.png')
plt.show()